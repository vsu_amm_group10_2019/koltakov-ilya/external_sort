﻿namespace Quick_Sort
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Run = new System.Windows.Forms.Button();
            this.upperLimit = new System.Windows.Forms.NumericUpDown();
            this.lowerLimit = new System.Windows.Forms.NumericUpDown();
            this.elementsCount = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upperLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowerLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsCount)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Info;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(776, 352);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Run
            // 
            this.Run.Location = new System.Drawing.Point(590, 22);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(188, 62);
            this.Run.TabIndex = 1;
            this.Run.Text = "Выполнить";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // upperLimit
            // 
            this.upperLimit.Location = new System.Drawing.Point(393, 48);
            this.upperLimit.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.upperLimit.Name = "upperLimit";
            this.upperLimit.Size = new System.Drawing.Size(145, 22);
            this.upperLimit.TabIndex = 5;
            this.upperLimit.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // lowerLimit
            // 
            this.lowerLimit.Location = new System.Drawing.Point(206, 48);
            this.lowerLimit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lowerLimit.Name = "lowerLimit";
            this.lowerLimit.Size = new System.Drawing.Size(145, 22);
            this.lowerLimit.TabIndex = 6;
            this.lowerLimit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // elementsCount
            // 
            this.elementsCount.Location = new System.Drawing.Point(15, 48);
            this.elementsCount.Name = "elementsCount";
            this.elementsCount.Size = new System.Drawing.Size(145, 22);
            this.elementsCount.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.elementsCount);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.lowerLimit);
            this.groupBox1.Controls.Add(this.upperLimit);
            this.groupBox1.Location = new System.Drawing.Point(12, 370);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(784, 94);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(390, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Максимальный предел";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(203, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Минимальный предел";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Количество элементов";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 476);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upperLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowerLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsCount)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Run;
        private System.Windows.Forms.NumericUpDown upperLimit;
        private System.Windows.Forms.NumericUpDown lowerLimit;
        private System.Windows.Forms.NumericUpDown elementsCount;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

