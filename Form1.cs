﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Quick_Sort
{
    public partial class Form1 : Form
    {

        public int[] array;

        public Form1()
        {
            InitializeComponent();
        }


        public int[] Sort(int[] array)
        {
            return Sort(array, 0, array.Length - 1);
        }
        private int[] Sort(int[] array, int minIndex, int maxIndex)
        {
            if (minIndex >= maxIndex)
            {
                return array;
            }
            int tmp = Partition(array, minIndex, maxIndex);
            Sort(array, minIndex, tmp - 1);
            Sort(array, tmp + 1, maxIndex);
            return array;
        }
        private int Partition(int[] array, int minIndex, int maxIndex)
        {
            int tmp = minIndex - 1;
            for (int i = minIndex; i < maxIndex; i++)
            {
                if (array[i] < array[maxIndex])
                {
                    tmp++;
                    Swap(array, tmp, i);
                }                
            }
            tmp++;
            Swap(array, tmp, maxIndex);
            drawSort(array, -1);
            drawMarking();
            Thread.Sleep(300);
            return tmp;
        }

        private void Run_Click(object sender, EventArgs e)
        {            
            Random random = new Random();
            array = new int[(int)elementsCount.Value];
            array = array.Select(x => random.Next((int)lowerLimit.Value, (int)(upperLimit.Value + 1))).ToArray();
            Sort(array);
        }

        public static void Swap(int[] array, int i, int j)
        {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }

        private void drawSort(int[] array, int a)
        {
            bool flag = true;
            Pen pen = new Pen(Color.Black);

            Graphics graphics = pictureBox1.CreateGraphics();
            graphics.Clear(Color.Wheat);
            for (int i = 1; i <= upperLimit.Value; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    {
                        if (flag)
                            pen = new Pen(Color.Azure);
                        else
                            pen = new Pen(Color.Chocolate);
                        flag = !flag;
                    }
                    if (array[j] >= i)
                        graphics.FillRectangle(new SolidBrush(pen.Color), 15 * j, pictureBox1.Height - 15 * i, 15, 15);
                }
            }
        }

        private void drawMarking()
        {
            Graphics graphics = pictureBox1.CreateGraphics();

            Pen pen = new Pen(Color.Black);
            for (int i = 0; i < pictureBox1.Height; i += 15)
                graphics.DrawLine(pen, 0, pictureBox1.Height - i, pictureBox1.Width, pictureBox1.Height - i);
            for (int i = 0; i < pictureBox1.Width; i += 15)
                graphics.DrawLine(pen, i, 0, i, pictureBox1.Width);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            ((PictureBox)sender).CreateGraphics().Clear(Color.Black);
            drawMarking();
        }
    }
}
